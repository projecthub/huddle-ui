import './home-header.css'
import React, { useRef, useLayoutEffect, useState, useEffect } from "react";
import SearchField from './search-field/search-field';
import useConfig from '../config';
import { useAvailableTagsQuery } from '../schemas';
import { useSearchParams } from 'react-router-dom';
import { Button, Popover } from '@nextui-org/react';
import { ArrowDropDown } from '@mui/icons-material';



const HomeHeaderDesktop: React.FC = (props) => {
    const [params, setParams] = useSearchParams()
    const [scrollPercentage, setScrollPercentage] = useState(0)
    const handleScroll = () => {
        setScrollPercentage(window.scrollY / (document.body.scrollHeight - window.innerHeight));
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll, { passive: true });

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <div className="home-header">
            <div style={{
                height: "30vh",
                display: "flex",
                flexDirection: "column",
                left: "30px",
                width: "40%",
                bottom: "10px",
                position: "absolute",
                maxWidth: "400px",
            }}>
                <div style={{ color: "white" }}>
                    <h1>Find your next</h1>
                    <h1>student project</h1>
                </div>
                <div style={{ flexGrow: scrollPercentage }} />

                <SearchField onChange={(searchString: string) => {
                    if (searchString === "") {
                        params.delete("search")
                    } else {
                        params.set("search", searchString)
                    }
                    setParams(params) // HomeHeader contains a search bar whose updates we can subscribe to here
                }} />

                <div style={{ flexGrow: 1 - scrollPercentage }} />
            </div>
            <CategoryList view='bubble' style={{ opacity: 1 - scrollPercentage }} />
            <CategoryList view='list' style={{ opacity: scrollPercentage }} />
        </div>
    );
}

export const HomeHeaderMobile: React.FC = () => {
    const [params, setParams] = useSearchParams()
    return (
        <div className='home-header-mobile'>
            <h1>Find your next student project</h1>
            <SearchField onChange={(searchString: string) => {
                if (searchString === "") {
                    params.delete("search")
                } else {
                    params.set("search", searchString)
                }
                setParams(params) // HomeHeader contains a search bar whose updates we can subscribe to here
            }} />
            <CategoryList view='list' />
        </div>
    )
}

const HomeHeader: React.FC = () => {
    const config = useConfig()
    switch (config.view) {
        case 'desktop':
            return <HomeHeaderDesktop />
        case 'mobile':
            return <HomeHeaderMobile />
    }
}
export default HomeHeader;

const CategoryButton = ({ category = "", activated = false, onClick = (() => { return }), marginLeft = 10 }) => {
    return (
        <div key={category}
            className={"home-header-category-button" + (activated ? " home-header-category-button-activated" : "")}
            onClick={onClick}
            style={{ marginLeft: marginLeft }}>
            <p>{category}</p>
        </div>
    );
}
function capitalizeFirstLetter(string = "") {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

type CategoryListProps = {
    view: "bubble" | "list"
    style?: React.CSSProperties
}
// You can consume the select category via callback or by reading the "category" search param
const CategoryList: React.FC<CategoryListProps> = (props) => {
    const [params, setParams] = useSearchParams()
    const category = params.get("category")
    const triggerCategory = (c: string) => {
        if (c === category) {
            params.delete("category")
        } else {
            params.set("category", c)
        }
        setParams(params)
    }
    const tagsData = useAvailableTagsQuery({ variables: { limit: 100 } })
    const categories = tagsData.data?.availableTags.map(t => t.name)
    switch (props.view) {
        case "bubble":
            return (
                <div style={{
                    position: "absolute",
                    right: "10px",
                    top: "20px",
                    width: "40vw",
                    height: "30vh",
                    display: "flex",
                    flexWrap: "wrap",
                    overflowY: "hidden",
                    alignItems: "flex-start",
                    alignContent: "flex-start",
                    ...props.style
                }}>
                    {categories?.slice(0, 9)?.map((c, i) => (
                        <CategoryButton category={capitalizeFirstLetter(c)}
                            onClick={() => triggerCategory(c)} activated={c === category} />))}
                </div>
            )
        case "list":
            return (
                <div style={{
                    position: "absolute",
                    right: "10px",
                    bottom: "10px",
                    display: "flex",
                    overflowX: "hidden",
                    alignItems: "flex-start",
                    alignContent: "flex-start",
                    overflow: "hidden",
                    ...props.style
                }}>
                    {categories?.slice(0, 3)?.map((c, i) => (
                        <CategoryButton category={capitalizeFirstLetter(c)}
                            onClick={() => triggerCategory(c)} activated={c === category} />))}
                    <div >
                        <Popover >
                            <Popover.Trigger>
                                <p style={{
                                    margin: 0,
                                    marginLeft: 20,
                                    top: "50%",
                                    transform: "translateY(50%)",
                                    cursor: "pointer",
                                }}>More <ArrowDropDown /></p>
                            </Popover.Trigger>
                            <Popover.Content>
                                {categories?.slice(3, 9)?.map((c, i) => (
                                    <CategoryButton category={capitalizeFirstLetter(c)}
                                        onClick={() => triggerCategory(c)} activated={c === category} />)) || []}
                            </Popover.Content>
                        </Popover>
                    </div>
                </div>
            )

    }
}
