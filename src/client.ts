import { HttpLink, ApolloClient, InMemoryCache, ApolloLink, Observable } from "@apollo/client";
import { offsetLimitPagination } from "@apollo/client/utilities";
import PushStream from "zen-push"
import { AuthenticationRequest } from "./authentication/authenticationObserbale";
import {clientConfig}  from "./config";
import { StrictTypedTypePolicies } from "./schemas";

const endpointLink = new HttpLink({
    uri: clientConfig.gqlUrl,
    credentials: "include"
});
// Subscribe to this to display the authentication popup when needing authentication
export const authenticationStream = new PushStream<AuthenticationRequest>();
const LoginLink = new ApolloLink((operation, forward) => {
    const observable = endpointLink.request(operation);
    let waitingForLogin = false
    return new Observable((observer) => {
        // subscribe to http link and handle authentication errors seperately
        const subs = observable!.subscribe({
            next: (data) => {
                if (data.errors?.[0]?.message.includes("authenticate")) {
                    waitingForLogin = true
                    // Ask for login
                    authenticationStream.next({
                        onFailed: () => {
                            observer.next(data)
                            waitingForLogin = false
                        },
                        onFinished: () => {
                            // retry fetching data after authentication
                            subs.unsubscribe()
                            endpointLink.request(operation)?.subscribe({
                                next: data => observer.next(data),
                                complete: () => observer.complete(),
                                error: (err) => observer.error(err)
                            })
                        }
                    })
                } else {
                    // forward data
                    observer.next(data);
                }
            },
            error: (error) => {
                observer.error(error);
            }
            ,
            complete: () => {
                if (!waitingForLogin) {
                    observer.complete();
                }
            }
        });
    });
});
const typePolicies: StrictTypedTypePolicies={
    Project:{
        keyFields:["id"],
        merge(existing, incoming) {
            return { ...existing, ...incoming };
        }
    },
    Query:{
        fields:{
            searchProjects:offsetLimitPagination(["options","searchString"])
        }
    }
}
export const client = new ApolloClient({
    // uri: 'https://huddle.ridilla.eu/api/query',
    cache: new InMemoryCache({typePolicies}),
    link: LoginLink
});