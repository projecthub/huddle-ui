import { Link, useNavigate } from 'react-router-dom';
import { useGetMeWithoutLoginPromptQuery, useGetProjectByIdQuery, useGetProjectUpdatesByIdQuery, useSaveProjectMutation, useUnsaveProjectMutation, useWriteMessageToProjectIdMutation } from '../schemas';
import { ImageGallery } from '../shared/ImageGallery';
import './project-detail.css'
import useConfig from '../config';
import Button from '../shared/Button';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm'
import TagsComponent from '../shared/TagsComponent/TagsComponent';
import { BookmarkAdd, BookmarkAdded, CloseOutlined } from '@mui/icons-material';
import { grey } from '@mui/material/colors';
import { UpdateList } from './UpdateList';
import { Status } from './Status';
export type ProjectDetailProps = {
    id: string
    onBackClicked?: () => void
    preventScroll?: boolean
}
const DetailElement: React.FC<{ description: string }> = (props) => {
    return (
        <div className='project-detail-element'>
            <p>{props.description}</p>
            <div>
                {props.children}
            </div>
        </div>
    )
}
const ProjectDetail: React.FC<ProjectDetailProps> = (props) => {
    const projectResult = useGetProjectByIdQuery({ variables: { id: props.id } });
    const projectUpdateResult = useGetProjectUpdatesByIdQuery({ variables: { id: props.id } });
    const meResult = useGetMeWithoutLoginPromptQuery();
    const navigate = useNavigate();
    const [saveProject] = useSaveProjectMutation()
    const [unsaveProject] = useUnsaveProjectMutation()
    const [writeMessageToProject] = useWriteMessageToProjectIdMutation()
    const config = useConfig()
    if (props.id == "") return <div></div>
    if (projectResult.loading || !projectResult.data?.getProject) return <div className='project-detail'>Loading...</div>
    if (projectResult.error) return <div className='project-detail'>Error: {projectResult.error.message}</div>
    const images = projectResult.data?.getProject?.images
    const twitter = projectResult.data?.getProject?.twitterUpdateSource
    return (
        <div className="project-detail" style={{
            width: config.view == "mobile" ? "100%" : "60%",
            overflowY:props.preventScroll ? "hidden" : undefined
        }}>
            {config.view == "mobile" && <div onClick={props.onBackClicked} className='close-container'><CloseOutlined fontSize='large' /></div>}
            <h1>{projectResult.data?.getProject?.name}</h1>
            <TagsComponent tags={projectResult.data?.getProject?.tags || ["...still loading tags"]} />
            <ReactMarkdown remarkPlugins={[remarkGfm]}>{projectResult.data?.getProject?.description || "(no description provided)"}</ReactMarkdown>
            {images && images.length > 0 ? <div >
                <ImageGallery images={images.map(image => ({
                    url: image.url,
                    description: image.description || undefined
                }))} />
            </div> : null}
            <p>Created by {projectResult.data?.getProject?.creator.username}</p>
            {meResult.data?.meIfLoggedIn && meResult.data?.meIfLoggedIn?.id == projectResult.data?.getProject?.creator.id ? <Link to={"/edit-project/" + props.id}>Edit</Link> : null}
            <Button filled onClick={() => {
                writeMessageToProject({ variables: { projectId: props.id, message: "Hi, I would like to connect!" } }).then(res => {
                    if (res.data?.writeMessageToProject) {
                        navigate("/messages")
                    }
                })
            }}>Connect</Button>
            {projectResult.data?.getProject?.saved ? <BookmarkAdded style={{ cursor: "pointer" }} fontSize="large" sx={{ color: grey[900] }} onClick={async () => {
                await unsaveProject({ variables: { projectId: props.id } })
                projectResult.refetch()
                projectResult.client.refetchQueries({
                    include: ["getSavedProjects"]
                })
            }} /> : <BookmarkAdd style={{ cursor: "pointer" }} fontSize="large" sx={{ color: grey[900] }} onClick={async () => {
                await saveProject({ variables: { projectId: props.id } })
                projectResult.refetch()
                projectResult.client.refetchQueries({
                    include: ["getSavedProjects"]
                })
            }} />}
            <h2>Details</h2>
            <div className='project-metadata-section'>
                <DetailElement description='Project status'><Status status={projectResult.data.getProject.status} /></DetailElement>
                <DetailElement description='Members'>{projectResult.data.getProject.member_count}</DetailElement>
                {twitter && <DetailElement description='Linked twitter account:'><a href={'https://twitter.com/' + twitter}>@{twitter}</a></DetailElement>}
            </div>
            {projectUpdateResult.data?.getProject?.updates && projectUpdateResult.data.getProject.updates.length > 0 && <UpdateList updates={projectUpdateResult.data.getProject?.updates.map(update => ({
                content: update.content,
                timestamp: new Date(update.time),
                images: update.images
            }))} />}
        </div>
    );
}

export default ProjectDetail;