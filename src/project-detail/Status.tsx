import { ProjectStatus } from "../schemas"
import CSS from 'csstype';
import "./status.css";
const StatusToColor: (s: ProjectStatus) => CSS.Property.BackgroundColor = (status) => {
    switch (status) {
        case ProjectStatus.Closed:
            return "red"
        case ProjectStatus.Idea:
            return "yellow"
        case ProjectStatus.Early:
            return "orange"
        case ProjectStatus.Established:
            return "green"
        case ProjectStatus.Suspended:
            return "gray"
    }
}
export const Status: React.FC<{ status: ProjectStatus }> = (props) => {
    return (
        <span className="status-tag" style={{ backgroundColor: StatusToColor(props.status) }}> {props.status}</span>
    )
}