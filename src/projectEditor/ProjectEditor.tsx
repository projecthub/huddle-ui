import { ProjectStatus } from "../schemas"
import loadable from '@loadable/component';
const MDEditor = loadable(() => import('@uiw/react-md-editor'))
import { useRef, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import { useAddImageMutation, useAddProjectTagMutation, useDeleteProjectMutation, useGetProjectByIdQuery, useGetProjectTwitterDatasourceQuery, useRemoveImageMutation, useRemoveProjectTagMutation, useSetProjectMemberCountMutation, useSetProjectStatusMutation, useSetProjectTwitterDatasourceMutation, useUpdateImageDescriptionMutation, useUpdateProjectDescriptionMutation, useUpdateProjectLocationMutation, useUpdateProjectNameMutation, useUpdateProjectVisibilityMutation } from "../schemas"
import Button from "../shared/Button"
import Input from "../shared/Input"
import "./projectEditor.css"
import useConfig from "../config";
import TagSelector from '../shared/tagSelector/TagSelector';
import FormControlLabel from '@mui/material/FormControlLabel';
import { CircularProgress, FormControl, InputLabel, MenuItem, Select, Switch } from '@mui/material';
import StacksEditor, { NewValueRequest } from '../shared/StacksEditor';
import PushStream from 'zen-push';

const statusList: ProjectStatus[] = []
for (const status in ProjectStatus) {
    statusList.push(status.toLowerCase() as ProjectStatus)
}

export const ProjectEditor: React.FC = props => {
    const params = useParams()
    const projectId = params["id"]
    const config = useConfig()
    const twitterAccount = useGetProjectTwitterDatasourceQuery({ variables: { projectId: projectId || "" } })
    const setDescriptionValuePushStream = useRef(new PushStream<string>())
    const projectData = useGetProjectByIdQuery({
        variables: { id: projectId || "" }, onCompleted: (data) => {
            setDescriptionValuePushStream.current.next(data.getProject?.description || "")
        }
    })
    const [setVisibility] = useUpdateProjectVisibilityMutation();
    const [removeTag] = useRemoveProjectTagMutation();
    const [addTag] = useAddProjectTagMutation();
    const [removeImage] = useRemoveImageMutation()
    const [updateImageDescription] = useUpdateImageDescriptionMutation()
    const [updateLocation] = useUpdateProjectLocationMutation()
    const [addImage] = useAddImageMutation()
    const [updateProjectName] = useUpdateProjectNameMutation()
    const [updateProjectDescription] = useUpdateProjectDescriptionMutation()
    const [deleteProject] = useDeleteProjectMutation()
    const [updateTwitter] = useSetProjectTwitterDatasourceMutation();
    const [updateMemberCount] = useSetProjectMemberCountMutation();
    const [updateProjectStatus] = useSetProjectStatusMutation();
    const navigate = useNavigate()
    const [newImageURL, setNewImageURL] = useState("")
    const [imageLoaded, setImageLoaded] = useState(false)
    if (!projectId) {
        return <Link to="/">Invalid URL! Go home</Link>
    }
    if (!projectData.data?.getProject) {
        return <CircularProgress />
    }

    return (
        <div className="project-editor">
            <h1>Edit project "{projectData.data?.getProject?.name}"</h1>

            <div className="project-editor-component">
                <h2>Settings</h2>
                <Input applyButton description="Project Name" initialValue={projectData.data?.getProject?.name} onApply={async (newName) => {
                    try {
                        const res = await updateProjectName({ variables: { newName: newName, projectId: projectId } })
                        if (res.data?.projectMutation?.updateName) {
                            projectData.refetch()
                            return { success: "Successfully updated project name!" }
                        } else {
                            return { error: res.errors?.join("") }
                        }
                    } catch (error) {
                        return { error: "Error updating project name" }
                    }
                }} />
                <Input applyButton description="Project Location" initialValue={projectData.data.getProject.location?.name} onApply={async (newLocation) => {
                    try {
                        const res = await updateLocation({ variables: { newLocation: { name: newLocation }, projectId: projectId } })
                        if (res.data?.projectMutation?.updateLocation) {
                            projectData.refetch()
                            return { success: "Successfully updated project location!" }
                        } else {
                            return { error: res.errors?.join("") }
                        }
                    } catch (error) {
                        return { error: "Could not update location" }
                    }

                }} />
                <hr />
                <Input applyButton description='Member count' type='number' initialValue={projectData.data.getProject.member_count.toString()} onApply={async (newCount) => {
                    try {
                        const res = await updateMemberCount({ variables: { memberCount: parseInt(newCount), projectId: projectId } })
                        if (res.data?.projectMutation?.updateMemberCount) {
                            projectData.refetch()
                            return { success: "Successfully updated member count" }
                        } else {
                            return { error: "Could not update member count:" + res.errors?.join("") }
                        }
                    } catch (error) {
                        return { error: "Could not update member count" }
                    }
                }} />
                <FormControl sx={{ m: 1, minWidth: 120 }}>
                    <InputLabel id="status-select">Project status</InputLabel>
                    <Select
                        labelId="status-select"
                        id="status-select"
                        value={projectData.data.getProject.status}
                        label="Project Status"
                        onChange={async (newStatus) => {
                            const res = await updateProjectStatus({ variables: { status: newStatus.target.value as ProjectStatus, projectId: projectId } })
                            projectData.refetch()
                            if (!res.data?.projectMutation?.updateStatus || res.errors) {
                                alert("Could not update project status")
                            }
                        }}
                    >
                        {statusList.map(status => <MenuItem value={status}>{status}</MenuItem>)}
                    </Select>
                </FormControl>
                <hr />
                <p>Add tags to help people interested in a category find your project</p>
                <TagSelector tags={projectData.data.getProject.tags} onNewTag={(newTag) => {
                    addTag({ variables: { projectId: projectId, tag: newTag } })
                        .then((res) => {
                            if (res.data?.projectMutation?.addTag) {
                                projectData.refetch()
                            } else {
                                alert(res.errors?.join(""))
                            }
                        }).catch((r) => {
                            alert("Could not add tag")
                        })
                }} onDeleteTag={(toDelete) => {
                    removeTag({ variables: { projectId: projectId, tag: toDelete } })
                        .then((res) => {
                            if (res.data?.projectMutation?.removeTag) {
                                projectData.refetch()
                            } else {
                                alert(res.errors?.join(""))
                            }
                        }).catch((r) => {
                            alert("Could not add tag")
                        })
                }} />
                <hr />
                <p>Link your twitter account for automatic updates (synced every full hour)</p>
                {twitterAccount.data?.getProject?.twitterUpdateSource && <p>Currently linked account: {twitterAccount.data.getProject.twitterUpdateSource}</p>}
                <Input applyButton description="Twitter username without @" clearOnEnter onApply={async (newVal) => {
                    const res = await updateTwitter({ variables: { projectId: projectId, username: newVal == "" ? undefined : newVal } })
                    if (res.data?.projectMutation?.setTwitterUpdateSource) {
                        projectData.refetch()
                        return { success: "Successfully linked twitter account! " }
                    }
                    return { error: "Could not update liked twitter account!" }

                }} />
                <hr />
                <p>Visibility of your project</p>
                <p><a href="mailto: tum-project-hub@protonmail.com">Reach out to us</a> if you want to make your project public</p>
                <FormControlLabel control={<Switch checked={projectData.data.getProject.public} onChange={(_, c) => {
                    setVisibility({ variables: { projectId: projectId, newVisibility: c } }).then((res) => {
                        if (!res.data?.projectMutation?.setVisibility) {
                            alert(res.errors?.join(""))
                        }
                        projectData.refetch()
                    }).catch((r) => {
                        alert("Could not update visibility")
                    })
                }} />} label={projectData.data.getProject.public ? "public" : "private"} />
            </div>
            <div className="project-editor-component">
                <h2>Description</h2>
                <StacksEditor setValueRequests={setDescriptionValuePushStream.current.observable} onSave={async newDescription => {
                    const updateRes = await updateProjectDescription({ variables: { newDescription, projectId: projectId } })
                    if (!updateRes.data?.projectMutation?.updateDescription) {
                        alert("Could not update description" + updateRes.errors?.join(""))
                    }
                    projectData.refetch()
                }} onLoad={() => {
                    setDescriptionValuePushStream.current.next(projectData.data?.getProject?.description || "")
                }
                } />
            </div>

            {projectData.data?.getProject?.images[0] && <div className="project-editor-component">
                <h2>Images</h2>
                <div className="image-list">
                    {projectData.data?.getProject?.images.map(image => {
                        return (
                            <div className="image-editor">
                                <img src={image.url} />
                                <p><Input applyButton description="Update Image Description" initialValue={image.description || undefined} onApply={async (newDescription) => {
                                    try {
                                        const res = await updateImageDescription({ variables: { projectId: projectId, newDescription: newDescription, imageId: image.id } })
                                        projectData.refetch()
                                        if (res.data?.projectMutation?.updateImageDescription) {
                                            return { success: "Successfully updated image description!" }
                                        } else {
                                            return { error: res.errors?.join("") }
                                        }
                                    } catch (error) {
                                        return { error: "Could not update image description" }
                                    }

                                }} /></p>
                                <p>
                                    <Button onClick={() => {
                                        removeImage({ variables: { projectId: projectId, imageId: image.id } })
                                            .then((res) => {
                                                if (res.data?.projectMutation?.removeImage) {
                                                    alert("Successfully removed image!")
                                                } else {
                                                    alert(res.errors?.join(""))
                                                }
                                                projectData.refetch()
                                            }).catch((r) => {
                                                alert("Could not remove image")
                                            })

                                    }} >Remove Image</Button>
                                </p>
                            </div>)
                    })}
                </div>
            </div>}
            <div className="project-editor-component">
                <h2>Add new image</h2>
                <Input description="Image URL" clearOnEnter onChange={(newVal) => {
                    setNewImageURL(newVal)
                    setImageLoaded(false)
                }} />
                <img src={newImageURL} onLoad={() => {
                    setImageLoaded(true)
                }} />
                {imageLoaded && <Button onClick={() => {
                    addImage({ variables: { projectId: projectId, newImage: { url: newImageURL } } })
                        .then((res) => {
                            if (res.data?.projectMutation?.addImage) {
                                alert("Successfully added image!")
                            } else {
                                alert(res.errors?.join(""))
                            }
                            projectData.refetch()
                        }).catch((r) => {
                            alert("Could not add image")
                        })
                }} >Add Image</Button>}
            </div>

            <div style={{ textAlign: "center" }}><Button confirm onClick={() => {
                deleteProject({ variables: { projectId: projectId } })

                    .then((res) => {
                        if (res.data?.projectMutation?.deleteProject) {
                            alert("Successfully deleted project!")
                            navigate("/")
                        } else {
                            alert(res.errors?.join(""))
                        }
                    }).catch((r) => {
                        alert("Could not delete project")
                    })
            }} >Delete Project</Button></div>
        </div>
    )
}