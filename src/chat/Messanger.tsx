import { ArrowBack, Close, TrafficOutlined } from "@mui/icons-material"
import { useState } from "react"
import { Chat, ChatParticipant } from "./ChatList"
import { UnifiedChat, UserChat } from "./Chat"
import { ChatList } from "./ChatList"
import "./messanger.css"
import useConfig from "../config"
export const Messanger: React.FC = props => {
    const [chosenChat, setChosenChat] = useState<Chat | undefined>(undefined)
    const config = useConfig()
    if (config.view == "mobile") {
        if (chosenChat) {
            return (
                <div className="chat-container-mobile">
                    <ArrowBack style={{
                        position: "absolute",
                        top: "10px",
                        left: "10px"
                    }} onClick={() => {
                        setChosenChat(undefined)
                    }} />
                    <UnifiedChat chat={chosenChat} />
                </div>
            )
        } else {
            return <ChatList onChatSelected={setChosenChat} />
        }
    }
    return (
        <div className="messanger">
            <div className="chat-container">
                <UnifiedChat chat={chosenChat} />
            </div>
            <ChatList onChatSelected={setChosenChat} />
        </div>
    )
}