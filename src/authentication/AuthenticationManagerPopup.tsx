import React, { useEffect, useState } from 'react';
import type { Identity, UiContainer, V0alpha2Api } from '@ory/kratos-client'
let _api: V0alpha2Api | undefined
export const getAPI = async () => {
    // allow for code splitting since the kratos sdk is large
    if (!_api) {
        const { V0alpha2Api, Configuration } = await import("@ory/kratos-client")
        const kratosConfig = new Configuration({
            basePath: clientConfig.kratosUrl,
            baseOptions: {
                withCredentials: true
            }
        });
        _api = new V0alpha2Api(kratosConfig);
    }
    return _api
}
import './AuthenticationManagerPopup.css'
import { AuthenticationRequest } from './authenticationObserbale';
import { Observable, useApolloClient } from '@apollo/client';
import Button from '../shared/Button';
import Input from '../shared/Input';
import { clientConfig } from "../config"
import { Link, useNavigate } from 'react-router-dom';
import { useGetMeWithoutLoginPromptQuery } from '../schemas';
export const AuthenticationManagerPopup: React.FC<{ a: Observable<AuthenticationRequest> }> = (props) => {
    const meData = useGetMeWithoutLoginPromptQuery()
    const client = useApolloClient()
    const [authRequests, setAuthRequests] = useState<AuthenticationRequest[]>([])
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    useEffect(() => {
        // listen for authentication requests on component creation...
        const subscription = props.a.subscribe(req => {
            setAuthRequests([...authRequests, req])
        })
        // ... and clean up when the component is unmounted
        return () => {
            subscription.unsubscribe()
        }
    }, [])
    if (authRequests.length === 0) {
        return null
    }
    return (
        <div id="authentication-manager-popup">
            <div>
                <h1>Authentication Panel</h1>
                {meData.data?.meIfLoggedIn && <p>Hello {meData.data.meIfLoggedIn?.username}, old friend!</p>}
                <br />
                <Input onChange={setEmail} description='email' autoComplete='email' />
                <br />
                <Input onChange={setPassword} description='Password' autoComplete='current-password' type='password' />

                <br />
                <Button filled disabledMessage={email.length === 0 || password.length === 0 ? "Please enter your email address and password above to log in" : undefined}
                    onClick={async () => {
                        try {
                            console.log('login');
                            const api = await getAPI()
                            const resp = await api.initializeSelfServiceLoginFlowForBrowsers()
                            console.log(resp.data);
                            if (!resp) return
                            const csrf_token = (resp.data.ui.nodes[0].attributes as { value: string }).value;

                            const loginResponse = await api.submitSelfServiceLoginFlow(resp.data.id, undefined, {
                                method: "password",
                                password: password,
                                password_identifier: email,
                                csrf_token: csrf_token
                            })
                            console.log(loginResponse.data);
                            authRequests.forEach(r => r.onFinished())
                            setAuthRequests([])
                            client.refetchQueries({ include: "all" })
                        } catch (error) {
                            alertUnknownError(error)
                        }
                    }}>login</Button>
                <p>or</p>
                <br />
                <Button filled disabledMessage={email.length === 0 || password.length === 0 ? "Please enter your email address and desired password above to register" : undefined}
                    onClick={async () => {
                        try {
                            console.log('registering user');
                            const api = await getAPI()
                            const resp = await api.initializeSelfServiceRegistrationFlowForBrowsers()
                            console.log(resp.data);
                            if (!resp) return
                            const registrationFlowId = resp.data.id;
                            const csrf_token = (resp.data.ui.nodes[0].attributes as { value: string }).value;

                            const registrationResponse = await api.submitSelfServiceRegistrationFlow(registrationFlowId, {
                                method: "password",
                                password: password,
                                traits: {
                                    email: email
                                },
                                csrf_token: csrf_token
                            })
                            console.log(registrationResponse.data);
                            resp.data.ui
                            api.initializeSelfServiceVerificationFlowForBrowsers().then(resp => {
                                console.log(resp.data);
                                const csrf_token = (resp.data.ui.nodes[0].attributes as { value: string }).value;
                                api.submitSelfServiceVerificationFlow(resp.data.id, undefined, {
                                    method: "link",
                                    email: email,
                                    csrf_token
                                }).then(resp => {
                                    console.log(resp.data);
                                    client.refetchQueries({
                                        include: "active",
                                    });
                                })
                            })
                        } catch (e) {
                            alertUnknownError(e)
                        }
                    }}>register</Button>
                <p>By registering, you consent to the <Link target="_blank" to="/about">data we collect and our terms of use</Link>. </p>
                <Button disabledMessage={email.length === 0 ? "Please enter your email address above to recover your account" : undefined}
                    onClick={async () => {
                        try {
                            console.log('trying to recover account');
                            const api = await getAPI()
                            const resp = await api.initializeSelfServiceRecoveryFlowForBrowsers()
                            console.log(resp.data);
                            if (!resp) return
                            const loginFlowId = resp.data.id;
                            const csrf_token = (resp.data.ui.nodes[0].attributes as { value: string }).value;

                            const recoveryResponse = await api.submitSelfServiceRecoveryFlow(resp.data.id, undefined, {
                                method: "link",
                                email,
                                csrf_token
                            })
                            console.log(recoveryResponse.data);
                            authRequests.forEach(r => r.onFinished())
                            setAuthRequests([])
                        } catch (error) {
                            alertUnknownError(error)
                        }
                    }}>reset password</Button>

                <br />
                <Button onClick={() => {
                    authRequests.forEach(r => r.onFailed("User closed window"))
                    setAuthRequests([])
                }}>close</Button>
            </div>
        </div>
    )
}

export const logout = async () => {
    console.log('logout');
    const api = await getAPI()
    const resp = await api.createSelfServiceLogoutFlowUrlForBrowsers()
    console.log(resp.data);
    if (!resp) return
    const logoutResponse = await api.submitSelfServiceLogoutFlow(resp.data.logout_token)
    console.log(logoutResponse.data);
}
export const LogoutButton: React.FC = props => {
    const navigate = useNavigate()
    return (
        <Button onClick={() => {
            logout()
            navigate("/")
        }}>Logout</Button>
    )
}

type ErrorWithUI = { response: { data: { ui: UiContainer } } }
const isMessageError = (e: unknown): e is ErrorWithUI => {
    return ((e as ErrorWithUI).response?.data?.ui?.messages?.length || 0) > 0
}
const alertUnknownError = (e: unknown) => {
    if (isMessageError(e)) {
        for (const message of e.response.data.ui.messages!) {
            alert(message.text)
        }
    } else {
        alert(e)
    }
}