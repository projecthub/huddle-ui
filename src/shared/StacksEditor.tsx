import { Observable } from "@apollo/client";
import type { StacksEditor as RawEditor } from "@olep/stacks-editor";
import { useEffect, useRef, useState } from "react";
import Button from "./Button";
export type NewValueRequest = {
    res: (newValue?: string) => void
}
let Editor: typeof RawEditor | undefined
const getEditor = async () => {
    if (!Editor) {
        const [e] =
            await Promise.all([
                import("@olep/stacks-editor"),
                import("@olep/stacks-editor/dist/styles.css"),
                import("@stackoverflow/stacks"),
                import("@stackoverflow/stacks/dist/css/stacks.css")])
        Editor = e.StacksEditor
    }
    return Editor
}
type StacksEditorProps = {
    setValueRequests: Observable<string>
    onSave?: (newValue: string) => Promise<void> | void
    onLoad?: () => void
}
const StacksEditor: React.FC<StacksEditorProps> = props => {
    const e = useRef<RawEditor | null>(null)
    const [editorReady, setEditorReady] = useState(false)
    const value = useRef("")
    useEffect(() => {
        const subs = props.setValueRequests.subscribe(newValue => {
            if (e.current) {
                e.current.content = newValue
            }
            value.current = newValue
        })
        return () => {
            subs.unsubscribe()
            e.current?.destroy()
        }
    }, [])
    useEffect(() => {
        getEditor().then((e) => {
            setEditorReady(true)
        })
    })
    if (!editorReady) {
        return <div>Loading</div>
    }
    return (
        <div>
            <div ref={(el) => {
                if (!Editor) {
                    return
                }
                if (el) {
                    if (!e.current) {
                        e.current = new Editor(el, value.current, {
                            editorHelpLink: "https://stackoverflow.com/editing-help"
                        })
                    } else {
                        e.current.enable()
                    }
                }
                props.onLoad?.()
            }}>
            </div>
            <Button onClick={async () => {
                e.current?.disable()
                value.current = e.current?.content || ""
                const saveRes = props.onSave?.(e.current?.content || "")
                if (saveRes) {
                    await saveRes
                }
                e.current?.enable()
            }}>Save</Button>
        </div>
    )
}
export default StacksEditor;