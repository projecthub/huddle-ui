import { Checkbox, FormControlLabel, Tooltip } from "@mui/material";
import { useState } from "react";
import "./Button.css";

type ButtonProps = {
    onClick?: () => void;
    filled?: boolean;
    disabledMessage?: string;
    confirm?: boolean;
}

const Button: React.FC<ButtonProps> = (props) => {
    const [checked, setChecked] = useState(false)
    if (props.confirm) {
        const { confirm, ...others } = props
        let message = props.children?.toString()
        if (message?.length) {
            message = message[0].toLowerCase() + message.slice(1)
        }
        return (
            <div>
                <FormControlLabel control={<Checkbox onChange={(_, c) => {
                    setChecked(c)
                }} />} label={"I really want to " + message} />
                <Button {...others} disabledMessage={checked ? undefined : "Check checkbox"} />
            </div>
        )
    }
    const classNames = "huddle-button" + (props.filled ? " huddle-button-filled" : " huddle-button-unfilled");
    if (props.disabledMessage) {
        return (
            <Tooltip title={props.disabledMessage}>
                <button className={classNames + " huddle-button-disabled"} onClick={() => {
                    alert(props.disabledMessage)
                }}>{props.children}</button>
            </Tooltip>
        )
    }
    return <button className={classNames} onClick={props.onClick}>{props.children}</button>
}
export default Button