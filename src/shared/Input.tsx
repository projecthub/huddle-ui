import { CircularProgress, Alert } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import Button from "./Button";
import "./Input.css";
import { Input as NextInput, useInput } from "@nextui-org/react"
import { margin } from "@mui/system";

export type InputApplyResult = {
    success?: string
    error?: string
}
type InputProps = {
    multiline?: boolean
    description?: string
    type?: string
    autoComplete?: string
    valueRef?: { value?: string }
    clearOnEnter?: boolean
    onApply?: (message: string) => (void | Promise<InputApplyResult | void>)
    style?: React.CSSProperties
    initialValue?: string
    enterOnUnfocus?: boolean
    applyButton?: boolean
    timeoutMS?: number
    hideHelp?: boolean
    onChange?: (value: string) => void
    clearable?: boolean
}

const Input: React.FC<InputProps> = (props) => {
    const v = useInput(props.initialValue || "")
    useEffect(() => {
        props.onChange?.(v.value)
    }, [v.value])
    const [waitingForConfirmation, setWaitingForConfirmation] = useState(false)
    const [response, setResponse] = useState<InputApplyResult | undefined>(undefined)
    const handleApply = async () => {
        if (waitingForConfirmation) {
            return
        }

        const applyRes = props.onApply?.(v.value || "")
        setWaitingForConfirmation(true)
        if (applyRes) {
            try {
                const res = await applyRes
                if (res) {
                    setResponse(res)
                    setTimeout(() => {
                        setResponse(undefined)
                    }, props.timeoutMS || 2000)
                }
            } catch (e) {
                alert(e)
            }
        }
        setWaitingForConfirmation(false)
        if (props.clearOnEnter) {
            v.setValue("")
        }
    }
    if (props.multiline) {
        return (
            <div>
                <textarea
                    value={v.value} placeholder={props.description}
                    className="huddle-input-multiline"
                    onChange={e => {
                        const value = e.target.value
                        if (props.valueRef) {
                            props.valueRef.value = value
                        }
                        v.setValue(value)
                    }
                    }
                />
                <Button filled={!waitingForConfirmation} onClick={handleApply}>Apply</Button>
                {response && <div className="alert-container">
                    <Alert onClose={() => {
                        setResponse(undefined)
                    }} severity={response.error ? "error" : "success"}>{response.error || response.success}</Alert>

                </div>}
            </div>
        )
    }
    const inputRef = useRef<HTMLInputElement>(null)
    return (
        <div className="huddle-input-container">
            <NextInput
                {...v.bindings}
                type={props.type}
                bordered
                onKeyPress={(e) => {
                    if (e.key == "Enter") {
                        handleApply()
                    }
                }}
                onBlur={e => {
                    if (props.enterOnUnfocus) {
                        handleApply()
                    }
                }
                }
                clearable={props.clearable}
                autoComplete={props.autoComplete}
                contentClickable={props.applyButton}
                labelPlaceholder={props.description}
                onContentClick={handleApply}
                contentRight={props.applyButton &&
                    (waitingForConfirmation ? <CircularProgress size={20} />
                        : <div>Ok</div>)} />
            {response && <div className="alert-container">
                <Alert onClose={() => {
                    setResponse(undefined)
                }} severity={response.error ? "error" : "success"}>{response.error || response.success}</Alert>

            </div>}
        </div>
    )
}
export default Input