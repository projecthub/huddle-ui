import { DeleteOutline } from "@mui/icons-material";
import { CircularProgress } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { getAPI, LogoutButton } from "../authentication/AuthenticationManagerPopup";
import ProjectList from "../project-list/project-list";
import { useGetMeQuery, useCreateProjectMutation, useSetMyUsernameMutation, useSetMyDescriptionMutation, useRemoveNotificationChannelMutation, useAddNotificationChannelMutation, useAddTelegramNotificationChannelMutation, ChannelId } from "../schemas";
import Button from "../shared/Button";
import Input from "../shared/Input";
import { Button as NextButton } from "@nextui-org/react";
import "./myProfile.css"

export const MyProfile: React.FC = (props) => {
    const { data, loading, refetch } = useGetMeQuery();
    const [setMyUsername] = useSetMyUsernameMutation()
    const [createProjectMutation] = useCreateProjectMutation();
    const [setMyDescription] = useSetMyDescriptionMutation();
    const [removeChannel] = useRemoveNotificationChannelMutation();
    const [addChannel] = useAddNotificationChannelMutation();
    const [addTelegramChannel] = useAddTelegramNotificationChannelMutation();
    const navigate = useNavigate()
    if (!data) {
        return <CircularProgress />
    }
    return (
        <div className="my-profile">
            <h1>My Profile</h1>
            <div className="profile-card">
                <div>Email: {data?.me.email}</div>
                <Input initialValue={data.me.username || undefined} applyButton description="Update username" enterOnUnfocus onApply={async (newUsername) => {
                    const res = await setMyUsername({ variables: { username: newUsername } })
                    if (res.data?.meMutation?.setUsername) {
                        refetch()
                        return { success: "Username updated" }
                    } else {
                        return { error: "Error updating username" }
                    }
                }} />
                <h4>About me</h4>
                {data?.me && <Input multiline initialValue={data.me.description} onApply={async (newDescription) => {
                    const res = await setMyDescription({ variables: { description: newDescription } })
                    if (res.data?.meMutation?.setDescription) {
                        refetch()
                        return { success: "Description updated" }
                    } else {
                        return { error: "Failed to update description" }
                    }
                }} />}
                <h4>My notification channels</h4>
                {data?.me?.notificationChannels?.map(c => (
                    <p>{c.channelId}: {c.channelUsername}<button onClick={async () => {
                        await removeChannel({ variables: { channelId: c.channelId } })
                        refetch()
                    }}><DeleteOutline /></button>
                    </p>))}
                <Input applyButton description="Add email for push notifications" onApply={async (newEmail) => {
                    await addChannel({ variables: { channelId: ChannelId.Email, username: newEmail } })
                    refetch()
                }} clearOnEnter clearable />
                <Input applyButton description="Add telegram username" onApply={async (telegramusername) => {
                    alert("Write any message to @huddle_notifier_bot to authorize push notifications to the account " + telegramusername)
                    await addTelegramChannel({ variables: { username: telegramusername } })
                    refetch()
                }} clearOnEnter clearable />
                <Input type="password" applyButton onApply={async (newPassword) => {
                    const api = await getAPI()
                    const settingsFlowData = await api.initializeSelfServiceSettingsFlowForBrowsers()
                    const csrf_token = (settingsFlowData.data.ui.nodes[0].attributes as { value: string }).value;
                    const settingsFlowId = settingsFlowData.data.id;
                    const submitSettingsFlowResponse = await api.submitSelfServiceSettingsFlow(settingsFlowId, undefined, {
                        method: "password",
                        password: newPassword,
                        csrf_token: csrf_token
                    })
                    alert("Password successfully updated")
                    console.log(submitSettingsFlowResponse)
                }} description="Update password" />
                <LogoutButton/>
            </div>
            <div className="profile-card">
                <h2>My projects</h2>
                <div className="my-profile-my-projects-container">
                    <ProjectList entries={data?.me.createdProjects?.map(p => ({
                        description: p.description,
                        name: p.name,
                        id: p.id
                    })) || []} />
                </div>
            </div>

            <p style={{ textAlign: "center" }}>
                <Button onClick={async () => {
                    const res = await createProjectMutation({ variables: { newProject: { name: "My new project", description: "", languages: [] } } })
                    if (res.data?.createProject) {
                        navigate(`/edit-project/${res.data.createProject.id}`)
                    }

                }}>Create new Project</Button>
            </p>
            <p style={{ textAlign: "center" }}>
                <Button confirm onClick={async () => {
                    throw new Error("not implemented")

                }}>Delete my account</Button>
            </p>
        </div>
    )

}