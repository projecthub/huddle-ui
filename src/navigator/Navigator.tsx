import { Link } from "react-router-dom"
import { AccountCircleOutlined,  BookmarkBorder, ChatOutlined, InfoOutlined } from "@mui/icons-material"
import { grey } from '@mui/material/colors';
import "./navigator.css";

export const Navigator: React.FC = () => {
    return (
        <div className="huddle-navigator">
            <Link to="/">Huddle</Link>
            <div className="huddle-navigator-right">
                <Link to='saved'><BookmarkBorder fontSize="inherit" sx={{ color: "white" }} /></Link>
                <Link to="/messages"><ChatOutlined fontSize="inherit" sx={{ color: "white" }} /></Link>
                <Link to="/profile"><AccountCircleOutlined fontSize="inherit" sx={{color: "white"  }} /></Link>
                <Link to="/about"><InfoOutlined fontSize="inherit" sx={{ color: "white" }} /></Link>
            </div>
        </div>
    )
}
// later for notifications
//<MarkUnreadChatAltOutlined/>