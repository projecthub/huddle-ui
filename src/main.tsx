import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import {
  ApolloProvider
} from "@apollo/client";
import { client } from './client';
import { BrowserRouter } from 'react-router-dom';
import { NextUIProvider } from '@nextui-org/react';
ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <NextUIProvider>
        <BrowserRouter basename='/ui'>
          <App />
        </BrowserRouter>
      </NextUIProvider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
