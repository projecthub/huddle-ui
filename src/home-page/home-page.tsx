import { useEffect, useRef, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import HomeHeader from '../home-header/home-header';
import ProjectList from '../project-list/project-list';
import { useSearchProjectsQuery } from '../schemas';
import "./home-page.css"

function HomePage() {
    const [params, setParams] = useSearchParams()
    const searchString = params.get("search") || ""
    const category = params.get("category") || undefined
    const [scrolledToBottom, setScrolledToBottom] = useState(false)
    useEffect(() => {
        const handleScroll = () => {
            const scrollPercentage = window.scrollY / (document.body.scrollHeight - window.innerHeight)
            if (scrollPercentage >= 0.98) {
                setScrolledToBottom(true)
            } else {
                setScrolledToBottom(false)
            }
        }
        window.addEventListener('scroll', handleScroll, { passive: true });

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);
    const getOptions = (cat?: string) => {
        if (cat)
            return { tag: cat }
    }
    const projectData = useSearchProjectsQuery({ variables: { searchString: searchString, limit: 10, options: getOptions(category) } })
    const lastRefetchOffset = useRef(-1)// keep track of the last offset we refetched to see if currently new data is loading already
    const onScrollToBottom = () => {
        if (lastRefetchOffset.current === projectData.data?.searchProjects?.length) {
            return;// already loading, so do nothing
        }
        lastRefetchOffset.current = projectData.data?.searchProjects?.length || -1;
        projectData.fetchMore({
            variables: {
                offset: projectData.data?.searchProjects?.length,
                limit: 10,
            }
        })
    }
    const entries = projectData.data?.searchProjects.map(p => ({
        description: p.description,
        id: p.id,
        name: p.name,
        location: p.location?.name,
        saved: p.saved,
        lastUpdated: Math.round((new Date().getTime() - new Date(p.createdAt).getTime()) / (1000 * 3600 * 24)).toString() + " days"
    })) || []
    return (
        <div style={{ position: "relative" }}>
            <HomeHeader />
            <div className='home-bottom'>
                <ProjectList entries={entries} preventScroll={!scrolledToBottom} onScrollToBottom={onScrollToBottom} />
            </div>
        </div>
    );
}

export default HomePage;