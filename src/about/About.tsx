import { ExpandMore } from "@mui/icons-material";
import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@mui/material";

const About: React.FC = () => {
    return (
        <div className="about">
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>About us</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        We are a student group at TUM with the goal of bringing students together in their personal projects.
                        <br />
                        Feel free to <a href="mailto: tum-project-hub@protonmail.com">reach out to us</a> for any questions, suggestions or feedback!
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Disclaimer</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Users can create projects on our platform whose content is not controlled by us and which does not necessarily represent our opinion. If you see anything disturbing, please <a href="mailto: tum-project-hub@protonmail.com">reach out to us</a>.
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Imprint</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Ole Petersen
                        <br />
                        Student Group Huddle
                        <br />
                        Technische Universität München
                        <br />
                        Studentische Vertretung
                        <br />
                        Arcisstr. 21
                        <br />
                        80333 München
                        <br />
                        <a href="mailto: tum-project-hub@protonmail.com">tum-project-hub@protonmail.com</a>
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Privacy policy</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <h2>What data do we collect</h2>
                        Generally, we collect all data that you store on Huddle. We must save e-mail addresses and usernames of the registered users and the users who filled in the registration form. The data is obtained by filling in the registration form or by editing personal data in the section Profile.
                        Huddle users may mark their favorite projects by saving them in the web app. Consequently, Huddle stores a list of favorite projects of every user.
                        <br />
                        Huddle users may create their own projects, which contain the following data:
                        <ul>
                            <li>Project title and description</li>
                            <li> Categories of the project (optional)</li>
                            <li> Address (optional)</li>
                            <li> Website and social (optional)</li>
                            <li> Person of contact (optional)</li>
                            <li> News and updates (optional)</li>
                        </ul>
                        Huddle also stores a list of projects created by the user.
                        Huddle does not collect any data about the users who did not fill in the registration form.
                        <h2>How will we use your data</h2>
                        Huddle uses the e-mail address of the user for the identification. Additionally, anyone can access the data saved in the project (as described in the section above), if the user creates the project and makes it public.
                        <br />
                        After filling in and sending the registration form, user will receive an e-mail with a confirmation link to confirm their identity. In the future Huddle may notify the users about a project update made by the project update by sending them an e-mail, if they saved the project to their favorites list. Users will always be able to delist any project from their project list or unsubscribe from the e-mail notifications in the Settings.
                        <br />
                        Huddle does not share your data with third parties. The only thing that may leak out is your public IP address when you consent to images being loaded from third parties.
                        If you wish us to find send you or delete all your data, <a href="mailto: tum-project-hub@protonmail.com">just send us an email</a>
                        <h2>How do we store your data</h2>
                        We store your data in our own database hosted at netcup and secure access to the data to our best knowledge. However do not enter any sensitive data on Huddle (for example in the chat)!
                        <h2>Cookies</h2>
                        We only use cookies that are technically necessary for authentication. We do not use any cookies when you are not logged in.
                        <h2>Privacy Policies of other websites</h2>
                        Projects in Huddle may contain links to other websites added by their respective owners. Our privacy policy applies only to this website.
                        <h2>Changes to this privacy policy</h2>
                        Huddle keeps its privacy policy under regular review and places any updates on this web page. This privacy policy was last updated on 24 April 2022.
                        <h2>How to contact us</h2>
                        If you have any questions about Huddle's privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to <a href="mailto: tum-project-hub@protonmail.com">reach out to us</a>.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Terms of use</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Do not share anything illegal or copyrighted on Huddle! Do not share inappropriate content such as pornography on our platform and do not claim to be someone else.
                        <br />
                        Also, feel free to <a href="mailto: tum-project-hub@protonmail.com">reach out to us</a> when you see illegal content.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Über uns</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Wir sind eine Plattform für Studierendenprojekte an der TUM.
                        <br />
                        <a href="mailto: tum-project-hub@protonmail.com">Kontaktiere uns</a> gerne für Fragen, Feedback oder Vorschläge!
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Impressum</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Ole Petersen
                        <br />
                        Studierendengruppe Huddle
                        <br />
                        Technische Universität München
                        <br />
                        Studentische Vertretung
                        <br />
                        Arcisstr. 21
                        <br />
                        80333 München
                        <br />
                        <a href="mailto: tum-project-hub@protonmail.com">tum-project-hub@protonmail.com</a>
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>Datenschutzerklärung</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <h2>Welche Daten sommeln wir?</h2>
                        Wir speichern alle Daten, die ihr auf unserer Website eingebt und später wieder aufrufen könnt. Bei Registrierung wird die Email-Adresse und der Benutzername gespeichert.
                        Auch die Favoritenprojekte werden gespeichert.
                        <br />
                        Nutzer können Projekte erstellen und wir speichern dabei folgende Daten
                        <ul>
                            <li>Name und Beschreibung</li>
                            <li> Tags</li>
                            <li> Ors</li>
                            <li> Website and social links</li>
                            <li> Kontakt (optional)</li>
                            <li> Updates (optional)</li>
                        </ul>
                        Huddle speichert auch wer welches Projekt erstellt hat.
                        Huddle speichert keine Daten über nicht registrierte Nutzer.
                        <h2>Wie wir deine Daten nutzen</h2>
                        Deine Emailadresse speichern wir zur Authentifizierung und optional zum Versenden von Benachrichtigungen. Projektdaten werden öffentlich, wenn du dein Projekt auf öffentlich stellst.
                        <br />
                        Wir teilen deine Daten nicht mit Third Parties. Nur deine IP-Adresse kann Third Parties durch das Laden von Bildern bekannt werden, wenn du dem zustimmst.
                        Wenn du möchtest, dass wir alle deine Daten löschen, <a href="mailto: tum-project-hub@protonmail.com">sende usn einfach eine Email.</a>
                        <h2>Wie wir deine Daten speichern</h2>
                        Wir speichern deine Daten auf unserer eigenen Datenbank, die bei netcup gehostet ist. Wir kümmern uns um die Sicherheit deiner Daten nach unserem besten Wissen und Gewissen. Trage trotzdem keine sensitiven Daten auf Huddle ein!
                        <h2>Cookies</h2>
                        Wir benutzen nur technisch notwendige Cookies zur Authentifizierung.
                        <h2>Datenschutzerklärungen anderer Websites</h2>
                        Projekte in Huddle können Links zu anderen Websites enthalten, auf denen wir nichts zu tun haben!
                        <h2>Changes to this privacy policy</h2>
                        Wir aktualisieren diese Policy regelmäßig, zuletzt am 24. April 2022.
                        <h2>Kontakt</h2>
                        Wenn du Fragen zu Huddles Datenschutzerklärung oder den gespeicherten Daten hast oder deine Datenschutzrechte ausübern möchtest, zögere nicht <a href="mailto: tum-project-hub@protonmail.com">uns zu kontaktieren</a>.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMore />}
                >
                    <Typography>AGB</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                       Teile nichts illegales, Urheberrechtlich geschütztes oder unpassendes (Pornographie...) auf unserer Plattform. Gib dich nicht als jemand anderes aus.
                        <br />
                        <a href="mailto: tum-project-hub@protonmail.com">Kontaktiere uns gerne</a>, wenn du etwas beunruhigendes auf unserer Seite siehst.
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </div >
    );
}
export default About;