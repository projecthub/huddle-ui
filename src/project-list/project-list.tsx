import { useEffect, useRef, useState } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import useConfig from "../config";
import ProjectDetail from "../project-detail/project-detail";
import ProjectListEntry, { ProjectListEntryProps } from "./project-list-entry/project-list-entry";
import './project-list.css'

export type ProjectListProps = {
    entries: Omit<ProjectListEntryProps, "key">[]
    onScrollToBottom?: () => void
    onScrollToTop?: () => void
    preventScroll?: boolean
}
const ProjectList: React.FC<ProjectListProps> = (props) => {
    const config = useConfig()
    const lastScrollTop = useRef(0)
    const scrollRef = useRef<HTMLDivElement>(null)
    const [params, setParams] = useSearchParams();
    const detailID = params.get("detail") || ""
    const setDetailID = (id: string) => {
        if (id == "") {
            params.delete("detail")
        } else {
            params.set("detail", id)
        }
        setParams(params)
    }
    const lastFirstId = useRef(props.entries[0]?.id || "")
    useEffect(() => {
        if (lastFirstId.current && lastFirstId.current !== props.entries[0]?.id) {
            switch (config.view) {
                case "desktop":
                    setDetailID(props.entries[0]?.id || "")
                    break;
                case "mobile":
                    setDetailID("")
            }
        }
        lastFirstId.current = props.entries[0]?.id
    })
    if (scrollRef.current) {
        if (scrollRef.current.scrollHeight === scrollRef.current.clientHeight && lastScrollTop.current === 0) {
            props.onScrollToBottom?.()
            lastScrollTop.current = scrollRef.current.scrollTop;
        }
        if (scrollRef.current.scrollTop === 0 && lastScrollTop.current !== 0) {
            props.onScrollToTop?.()
            lastScrollTop.current = scrollRef.current.scrollTop;
        }
    }
    useEffect(() => {
        if (detailID == "" && props.entries.length > 0 && config.view == "desktop") {
            setDetailID(props.entries[0].id || "");
        }
    })
    if (config.view === "mobile") {
        if (detailID != "") {
            return <ProjectDetail id={detailID} onBackClicked={() => {
                setDetailID("")
            }} />
        }
        return (
            <div className="project-list" style={{
                width: "100%",
                overflowY: props.preventScroll ? "hidden" : undefined
            }} onScroll={(ev) => {
                const distanceToBottom = ev.currentTarget.scrollHeight - ev.currentTarget.scrollTop - ev.currentTarget.offsetHeight;
                const lastDistanceToBottom = ev.currentTarget.scrollHeight - lastScrollTop.current - ev.currentTarget.offsetHeight;
                if (distanceToBottom < 100 && lastDistanceToBottom > 100) {
                    lastScrollTop.current = ev.currentTarget.scrollTop;
                    props.onScrollToBottom?.();
                }
            }} ref={scrollRef}>
                {props.entries.map((entry, i) => <ProjectListEntry {...entry} key={i} onClick={(id => {
                    setDetailID(id);
                })} />)}
            </div>)
    }
    return (
        <div className="project-browser">
            <div className="project-list" style={props.preventScroll ? {
                overflowY: "hidden"
            } : undefined} 
            
            onScroll={(ev) => {
                const distanceToBottom = ev.currentTarget.scrollHeight - ev.currentTarget.scrollTop - ev.currentTarget.offsetHeight;
                const lastDistanceToBottom = ev.currentTarget.scrollHeight - lastScrollTop.current - ev.currentTarget.offsetHeight;
                if (distanceToBottom < 100 && lastDistanceToBottom > 100) {
                    lastScrollTop.current = ev.currentTarget.scrollTop;
                    props.onScrollToBottom?.();
                }
            }} ref={scrollRef}>
                {props.entries.map((entry, i) => <ProjectListEntry {...entry} key={i} onClick={(id => {
                    setDetailID(id);
                })} />)}
            </div>
            <ProjectDetail id={detailID} preventScroll={props.preventScroll} />
        </div>
    );
}



export default ProjectList;